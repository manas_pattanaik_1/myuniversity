using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyUniversity.Models;
using MyUniversity.Models.SchoolViewModels;

namespace MyUniversity.Pages.Dept
{
    public class IndexModel : PageModel
    {
        private readonly MyUniversity.Models.SchoolContext _context;

        public IndexModel(MyUniversity.Models.SchoolContext context)
        {
            _context = context;
        }

        public IList<Department> Department { get;set; }

        public DepartmentDetailsData DepartmentDetails{get;set;}

        public async Task OnGetAsync(int? id,int? cid)
        {
            DepartmentDetails = new DepartmentDetailsData();

            DepartmentDetails.Departments = await _context.Departments
                                .Include(d => d.Administrator).
                                Include(d=>d.Courses).
                                    ThenInclude(i=>i.Enrollments).
                                    ThenInclude(i=>i.Student).                                  
                                ToListAsync();

            if(id!=null) {
                var did=id.Value;
                var dept = DepartmentDetails.Departments.Single(d=>d.DepartmentID==did);
                DepartmentDetails.Courses = dept.Courses;
            }      

            if(cid!=null) {
                
                var course = DepartmentDetails.Courses.Single(d=>d.CourseID==cid.Value);
                DepartmentDetails.Enrollements =  course.Enrollments;
            }              

        }
    }
}
