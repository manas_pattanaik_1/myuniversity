using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MyUniversity.Models;
using MyUniversity.Models.Utility;

namespace MyUniversity.Pages.Admission
{
    public class CreateModel : PageModel
    {
        private readonly MyUniversity.Models.SchoolContext _context;

        public CreateModel(MyUniversity.Models.SchoolContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {   
            ViewData["CourseID"] = new SelectList(_context.Courses, "CourseID", "Title");
            ViewData["StudentID"] = new SelectList(_context.Student, "ID", "FirstMidName");
            var gradeList = new SelectList(EnumToSelectList.GetGrades(),"key","value");
            ViewData["Grade"] = gradeList;
            return Page();
        }    


        [BindProperty]
        public Enrollment Enrollment { get; set; }

        [BindProperty]
        public Grade? Grade{get;set;}

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Enrollment.Add(Enrollment);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}