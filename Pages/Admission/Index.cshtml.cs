using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyUniversity.Models;

namespace MyUniversity.Pages.Admission
{
    public class IndexModel : PageModel
    {
        private readonly MyUniversity.Models.SchoolContext _context;

        public IndexModel(MyUniversity.Models.SchoolContext context)
        {
            _context = context;
        }

        public IList<Enrollment> Enrollment { get;set; }

        public async Task OnGetAsync()
        {
            Enrollment = await _context.Enrollment
                .Include(e => e.Course)               
                .Include(e => e.Student)
                .ToListAsync();
        }
    }
}
