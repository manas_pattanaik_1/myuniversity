using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MyUniversity.Models;
using MyUniversity.Models.SchoolViewModels;

namespace MyUniversity.Pages.Courses
{
    public class IndexModel : PageModel
    {
        private readonly MyUniversity.Models.SchoolContext _context;

        public IndexModel(MyUniversity.Models.SchoolContext context)
        {
            _context = context;
        }

        public IList<Course> Course { get;set; }

        public IList<CourseViewModel> CourseVM { get; set; }

        public async Task OnGetAsync()
        {
            // Course = await _context.Courses
            //                 .Include(c => c.Department)
            //                 .AsNoTracking()
            //                 .ToListAsync();

            CourseVM = await _context.Courses
                            .Select(p => new CourseViewModel
                            {
                                CourseID = p.CourseID,
                                Title = p.Title,
                                Credits = p.Credits,
                                DepartmentName = p.Department.Name
                            }).ToListAsync();
        }
    }
}
