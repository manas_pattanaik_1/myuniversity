using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MyUniversity.Models.Utility
{ 
public static class EnumToSelectList{

        public static IDictionary<int,string> GetGrades()
        {
            return Enum.GetValues(typeof(Grade))
                        .Cast<Grade>()
                        .ToDictionary(key => (int)key, value => value.ToString() );                            
        }       
    }
}