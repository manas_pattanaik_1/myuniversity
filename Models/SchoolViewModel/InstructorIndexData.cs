using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyUniversity.Models;

namespace MyUniversity.Models.SchoolViewModels
{
    public class InstructorIndexData
    {
        public IEnumerable<Instructor> Instructors { get; set; }
        public IEnumerable<Course> Courses { get; set; }
        public IEnumerable<Enrollment> Enrollments { get; set; }
    }

    public class DepartmentDetailsData{
        public IEnumerable<Department> Departments {get;set;}
        public IEnumerable<Course> Courses{get;set;}

        public IEnumerable<Enrollment> Enrollements {get;set;}
    }
}